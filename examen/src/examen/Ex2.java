package examen;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;


public class Ex2 extends JFrame {
    JTextField text;
    JButton buton;
    Ex2(){

        setTitle("Exercitiul 2");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(400,400);
        display();
        setVisible(true);
    }


    private void display(){
        this.setLayout(null);

        buton = new JButton("Press");
        buton.setBounds(50,20,80,20);

        text = new JTextField();
        text.setBounds(50,60,150,30);

        buton.addActionListener(new ButtonAction());

        add(buton);
        add(text);
        getContentPane();
    }
    public static void main(String[] args)
    {
        new Ex2();
    }
    class ButtonAction implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
             //  E:\\examen_isp_2021\\examen\\src\\examen\\text.txt- I used this value in the text field, I created text.txt and wrote some text in it
            String name=text.getText();

            try
            {
                File file = new File(name);
                BufferedReader br = new BufferedReader(new FileReader(file));
                String st;
                String full = "";
                while ((st = br.readLine()) != null)
                {
                    full+=st;
                    full+="\n";
                }
                String reverse = "";

                for (int i = full.length() - 1; i >= 0; i--) {
                    reverse = reverse + full.charAt(i);
                }

                System.out.println(reverse);

            }
            catch(Exception e2) { System.out.println(e2); }

        }
    }
}
